CC := cc

SOURCES := src
HEADERS := headers
TESTS   := tests
TARGET  := target
TEST    := $(TARGET)/test
RELEASE := $(TARGET)/release
DEBUG   := $(TARGET)/debug

CFLAGS := -Wall -I$(HEADERS)
CFLAGS_DEBUG   := $(CFLAGS) -g -O0 -DDEBUG_BUILD
CFLAGS_RELEASE := $(CFLAGS) -Werror -O2 -march=native
CFLAGS_TEST    := $(CFLAGS_DEBUG)

LIBS := -lpng -lpthread -lm

BIN := $(notdir $(shell pwd))

SRCS := $(shell ls $(SOURCES))
OBJS := $(SRCS:.c=.o)
SRCS := $(addprefix $(SOURCES)/, $(SRCS))

DEBUG_OBJS   := $(addprefix $(DEBUG)/,   $(OBJS))
RELEASE_OBJS := $(addprefix $(RELEASE)/, $(OBJS))
TEST_OBJS    := $(addprefix $(TEST)/,    $(OBJS:main.o=))

TEST_SRCS := $(shell ls $(TESTS))
TEST_BINS := $(addprefix $(TEST)/, $(TEST_SRCS:.c=))
TEST_SRCS := $(addprefix $(TESTS)/, $(TEST_SRCS))

PREFIX := /usr/local

debug: $(DEBUG)/$(BIN)

# sources and headers' dependencies
$(SOURCES)/main.c: $(HEADERS)/blur_pixel_buffer.h
	touch $@

$(SOURCES)/blur_pixel_buffer.c: $(HEADERS)/blur_pixel_buffer.h \
	$(HEADERS)/config.h
	touch $@

$(DEBUG):
	mkdir -p $@

$(DEBUG)/%.o: $(SOURCES)/%.c | $(DEBUG)
	$(CC) $(CFLAGS_DEBUG) -c -o $@ $< $(LIBS)

$(DEBUG)/$(BIN): $(DEBUG_OBJS)
	$(CC) $(CFLAGS_DEBUG) -o $@ $^ $(LIBS)

release: $(RELEASE)/$(BIN)

$(RELEASE):
	mkdir -p $@

$(RELEASE)/%.o: $(SOURCES)/%.c | $(RELEASE)
	$(CC) $(CFLAGS_RELEASE) -c -o $@ $< $(LIBS)

$(RELEASE)/$(BIN): $(RELEASE_OBJS)
	$(CC) $(CFLAGS_RELEASE) -o $@ $^ $(LIBS)

test: $(TEST_BINS)

$(TEST):
	mkdir -p $@

$(TEST)/%.o: $(SOURCES)/%.c | $(TEST)
	$(CC) $(CFLAGS_TEST) -c -o $@ $< $(LIBS)

$(TEST)/%: $(TESTS)/%.c $(TEST_OBJS) | $(TEST)
	$(CC) $(CFLAGS_TEST) -o $@ $^ $(LIBS)

install: $(RELEASE)/$(BIN)
	mkdir -p $(PREFIX)/bin
	cp $< $(PREFIX)/bin

uninstall:
	rm $(PREFIX)/bin/$(BIN)
	# rm -d $(PREFIX)/bin

options:
	@echo $(TEST_SRCS)
	@echo $(TEST_OBJS)
	@echo $(TEST_BINS)

all: debug release test

clean:
	rm -rf $(TARGET)

.PHONY: all clean options debug release test install
