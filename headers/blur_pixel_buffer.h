#include <stdint.h>

typedef unsigned char *buffer_t;

extern int blur_pixel_buffer(const buffer_t source, buffer_t dest, uint32_t w,
		uint32_t h, uint8_t channels, uint32_t radius);
