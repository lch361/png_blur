#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <time.h>

#include <png.h>

#include "blur_pixel_buffer.h"

#define min(_a, _b) ((_a) < (_b) ? (_a) : (_b))
#define max(_a, _b) ((_a) > (_b) ? (_a) : (_b))

void die(int code, const char *message) {
	fputs(message, stderr);
	exit(code);
}

int main(int argc, const char **argv) {
	if (argc < 2) {
		die(-1, "Not enough arguments. Should be at least 2.\n");
	}

	uint32_t radius;
	{
		if (argv[1][0] == '-') {
			die(-1, "Radius can not be negative.\n");
		}
		char *endptr = NULL;
		radius = strtoul(argv[1], &endptr, 10);
		if (*endptr != '\0') {
			die(-1, "Radius should be an integer number.\n");
		}
	}

	png_image source = {
		.version = PNG_IMAGE_VERSION,
		.opaque = NULL,
	};

	png_image_begin_read_from_stdio(&source, stdin);
	if (source.format & PNG_FORMAT_FLAG_COLORMAP) {
		die(-1, "Colormapped pngs are not supported.");
	}

	uint8_t channels = 3; // TODO: channels are dependant on format

	buffer_t buffer = malloc(source.width * source.height * channels);
	png_image_finish_read(&source, NULL, buffer, 0, NULL);

	buffer_t new_buffer = malloc(source.width * source.height * channels);
	blur_pixel_buffer(buffer, new_buffer, source.width, source.height,
			channels, radius);
	free(buffer);

	png_image dest = {
		.version = source.version,
		.flags   = source.flags,
		.format  = source.format,
		.width   = source.width,
		.height  = source.height,
		.opaque  = NULL
	};

	png_image_write_to_stdio(&dest, stdout, 0, new_buffer, 0, NULL);
	free(new_buffer);

	return 0;
}
