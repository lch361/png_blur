#include <stdlib.h>
#include <stdint.h>
#include <pthread.h>
#include <math.h>
#include <stdio.h>

#include "config.h"

#include "blur_pixel_buffer.h"

#define max(a, b) ((a) > (b) ? (a) : (b))
#define min(a, b) ((a) < (b) ? (a) : (b))
#define abs(a) ((a) > 0 ? (a) : -(a))

typedef unsigned char *buffer_t;
typedef _Bool bool;

// Offsets to form a circle
static uint32_t *radius_offsets;

// Remake this to use radius_offsets
void blur_pixel(uint64_t *prefixes, buffer_t dest,
		uint32_t x, uint32_t y, uint32_t w,
		uint32_t h, uint8_t channels, uint32_t radius) {

	const bool are_columns = h > w;

	for (uint8_t c = 0; c < channels; ++c) {
		uint64_t sum = 0, surface = 0;
		uint32_t offset, p_begin, p_end;
		if (are_columns) {
			for (uint32_t i = max((int32_t)x - (int32_t)radius, 0);
					i <= min(w - 1, x + radius); ++i) {
				offset = radius_offsets[abs((int32_t)i - (int32_t)x)];
				p_begin = max(0, (int32_t)y - (int32_t)offset);
				p_end   = min(h - 1, y + offset);
				surface += p_end - p_begin + 1;

				sum += prefixes[p_end * w * channels + i * channels + c] -
					(p_begin == 0 ? 0 : prefixes[(p_begin - 1) * w * channels +
					i * channels + c]);
			}
		}
		else {
			for (uint32_t i = max((int32_t)y - (int32_t)radius, 0);
					i <= min(h - 1, y + radius); ++i) {
				offset = radius_offsets[abs((int32_t)i - (int32_t)y)];
				p_begin = max(0, (int32_t)x - (int32_t)offset);
				p_end   = min(w - 1, x + offset);
				surface += p_end - p_begin + 1;

				sum += prefixes[i * w * channels + p_end * channels + c] -
					(p_begin == 0 ? 0 : prefixes[i * w * channels +
					(p_begin - 1) * channels + c]);
			}
		}

		dest[y * channels * w + x * channels + c] = sum / surface;
	}
}

typedef struct {
	buffer_t source;
	uint64_t *dest;
	uint32_t w, h;
	uint8_t channels;
	uint32_t start, end;
} MakePrefixesArgs;

void *make_prefixes(void *argv) {
	MakePrefixesArgs *args = argv;
	const bool are_columns = args->h > args->w;

	for (uint32_t i = args->start; i < args->end; ++i) {
		const uint64_t
		start_j = are_columns ? i : args->w * i,
		end_j   = start_j + (are_columns ? args->h * args->w : args->w),
		inc_j   = are_columns ? args->w : 1;

		for (uint8_t c = 0; c < args->channels; ++c) {
			args->dest[start_j * args->channels + c] =
				args->source[start_j * args->channels + c];
			for (uint64_t j = start_j + inc_j; j < end_j; j += inc_j) {
				args->dest[j * args->channels + c] =
					args->dest[(j - inc_j) * args->channels + c] +
					args->source[j * args->channels + c];
			}
		}
	}

	return NULL;
}

typedef struct {
	uint64_t *prefixes;
	buffer_t dest;
	uint32_t w, h;
	uint8_t channels;
	uint32_t radius;
	uint64_t start, end;
} BlurPixelChunkArgs;

void *blur_pixel_chunk(void *argv) {
	BlurPixelChunkArgs *args = argv;

	for (uint64_t i = args->start; i < args->end; ++i) {
		uint32_t y = i / args->w, x = i % args->w;
		// printf("%u %u\n", x, y); fflush(stdout);
		blur_pixel(args->prefixes, args->dest, x, y, args->w, args->h,
				args->channels, args->radius);
	}

	return NULL;
}

void initialize_radius_offsets(uint32_t r) {
	radius_offsets = malloc((r + 1) * sizeof(uint32_t));
	radius_offsets[0] = r;
	for (uint32_t i = 1; i <= r; ++i) {
		radius_offsets[i] = ceil(sqrt((r + 1) * (r + 1) - i * i)) - 1;
	}
}

int blur_pixel_buffer(const buffer_t source, buffer_t dest, uint32_t w,
		uint32_t h, uint8_t channels, uint32_t radius) {

	initialize_radius_offsets(radius);
	if (radius_offsets == NULL) return -1;

	uint64_t *prefixes = malloc(w * h * channels * sizeof(uint64_t));
	if (prefixes == NULL) return -1;

	pthread_t threads[PROCS] = {};

	{
		MakePrefixesArgs *mp_args = malloc(sizeof(MakePrefixesArgs) * PROCS);
		const uint32_t mp_chunk_length = min(w, h) / PROCS;

		for (uint32_t i = 0; i < PROCS; ++i) {
			mp_args[i].channels = channels;
			mp_args[i].h        = h;
			mp_args[i].w        = w;
			mp_args[i].source   = source;
			mp_args[i].dest     = prefixes;
			mp_args[i].start    = mp_chunk_length * i;
			mp_args[i].end      = (i == PROCS - 1) ? min(w, h) :
				mp_chunk_length * (i + 1);
			pthread_create(threads + i, NULL, make_prefixes, mp_args + i);
		}
		for (uint32_t i = 0; i < PROCS; ++i) {
			pthread_join(threads[i], NULL);
			threads[i] = 0;
		}
		free(mp_args);
	}

	{
		BlurPixelChunkArgs *bpc_args = malloc(sizeof(BlurPixelChunkArgs) * PROCS);
		const uint64_t bpc_chunk_length = w * h / PROCS;

		for (uint32_t i = 0; i < PROCS; ++i) {
			bpc_args[i].prefixes = prefixes;
			bpc_args[i].dest     = dest;
			bpc_args[i].w        = w;
			bpc_args[i].h        = h;
			bpc_args[i].channels = channels;
			bpc_args[i].radius   = radius;
			bpc_args[i].start    = bpc_chunk_length * i;
			bpc_args[i].end      = (i == PROCS - 1) ? w * h :
				bpc_chunk_length * (i + 1);
			pthread_create(threads + i, NULL, blur_pixel_chunk, bpc_args + i);
		}
		for (uint32_t i = 0; i < PROCS; ++i) {
			pthread_join(threads[i], NULL);
		}
		free(bpc_args);
	}

	free(prefixes);
	free(radius_offsets);

	return 0;
}
